package com.castellanos.repasorv_yefer;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

// esta clase es un ViewHolder va a ser el cajon para cargar datos del reciclerView
// esto hereda
public class Elemento  extends RecyclerView.ViewHolder {

    //crear un elemento de tipo text view

    TextView txtElemento, txtElementoD;


    //crear el constructor para pasar el layout

    //itemView es el elemento referenciado

    public Elemento(@NonNull View itemView) {
        super(itemView);

        //hacer la referencia entre el elemegto que se cero en el lbjeto con el layout
        //crear un objeto de la clase elemento

        txtElemento= itemView.findViewById(R.id.txtElemento);
        txtElementoD= itemView.findViewById(R.id.txtElemento);
    }
}